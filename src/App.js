/*
Author: Danh Mai
Last update: 9:35 03/24/2019 by Minh Hung-VU
Review: by Quang Nguyen
- Remove title in left container
- Fix UI 
Summary: None
Bug: None
Ongoing: None
Checlist: 
[x] magnifier demo
[x] filter demo
[x] image processing toolbar
[x] image processing display result
*/

import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <div>Add Login demo</div>
          <div>Login form</div>
          <div>Remove Firebase UI</div>
          <div>Magnifier</div>
          <div>Filter</div>
          <div>Image processing toolbar</div>
        </header>
      </div>
    );
  }
}

export default App;
