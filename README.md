# Demo Project

## Requirement
**Dependencies**

Node version 10.0

React version 16.8.5

Native-base version 2.12.1

Firebase version 2.0.0

**Development Plugin**

Eslint version 0.54.2

Prettier version 0.2.1


## Environment
### Device
1. MacOS
- Version 10.14.04
- CPU 2.3GHz Intel Core i5
- Ram 8GB
- SSD 256GB
2. Window
- Window 10 Pro
- CPU 2,3GHz Intel Core i7
- Ram 4GB
- HDD 1TB
### Browser
1. Chrome 73.0
2. Safari 12.0.3

## Build script
1. Clone project/Pull branch
2. Install dependencies
```
npm install
```
3. Build script
- Localhost:
```
npm start
```
- Deployment
```
npm run build
```
## Version

## Features
### Login
- **Username**: string, email
- **Password**: string | min: 6 characters, password
### Image processing toolbar
**Input**
- Image: file | width <1024px | height <1024px, fundus image
**Output**
- Output Image: file | width = 640px | height = 1024, filterd image
